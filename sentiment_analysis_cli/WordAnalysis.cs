﻿using System;
namespace sentiment_analysis_cli
{
    public class WordAnalysis
    {
        public void calculateSentiment() {
            // Read file contents
            System.IO.StreamReader reviewFile;
            try
            {
                reviewFile = new System.IO.StreamReader(@"../../movieReviews.txt");
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("ERROR! File Not Found!");
                reviewFile = null;
                Console.WriteLine("Exiting...");
                Environment.Exit(-1);
            }
            // Ask user for input
            Console.WriteLine("Write a string: ");
            string word = Console.ReadLine();

            string line;
            int numFound = 0, totalScore = 0;

            // Variable for current sentiment after splitting
            int currentSentiment = 0;
            string currentReview;

            // Read new line until end of file reached
            while ((line = reviewFile.ReadLine()) != null)
            {
                // Divide line into two
                currentSentiment = Int32.Parse(line.Substring(0, line.IndexOf(' ')));
                currentReview = line.Substring(line.IndexOf(' ') + 1);
                // Check if review contains key word
                if (currentReview.Contains(word))
                {
                    // Increment sentiment and number of times word was found
                    totalScore += currentSentiment;
                    numFound += 1;
                }
            }
            // Compute average score
            double avgScore;
            try
            {
                avgScore = (double)totalScore / (double)numFound;
                //Console.WriteLine(totalScore / numFound);
            }
            catch (DivideByZeroException)
            {
                avgScore = 0.0;
            }
            Console.WriteLine($"{word} appears: {numFound:.0} times");
            Console.WriteLine($"The average score for reviews for {word} is {avgScore:0.00000}");
        }
    }
}
