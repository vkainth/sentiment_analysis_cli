﻿namespace sentiment_analysis_cli
{
    class ReviewDataStruct
    {
        public int sentiment { get; }

        public string review { get; }

        public ReviewDataStruct(int _sentiment, string _review)
        {
            sentiment = _sentiment;
            review = _review;
        }
        public ReviewDataStruct() {
            sentiment = -1;
            review = string.Empty;
        }
    }
}
