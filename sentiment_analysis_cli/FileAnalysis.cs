﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace sentiment_analysis_cli
{
    public class FileAnalysis
    {
        public void calculateFileSentiment()
        {
            // Ask user for file
            Console.WriteLine("Enter name of file you want for which you want the average score");
            string fileName = Console.ReadLine();
            // Try to read the file
            System.IO.StreamReader inputFile;
            try
            {
                inputFile = new System.IO.StreamReader($"../../{fileName}");
            }
            catch (System.IO.FileNotFoundException)
            {
                inputFile = null;
                Console.WriteLine("ERROR! File Not Found!");
                Console.WriteLine("Exiting...");
                Environment.Exit(-1);
            }
            // Read file contents
            System.IO.StreamReader reviewFile;
            try
            {
                reviewFile = new System.IO.StreamReader(@"../../movieReviews.txt");
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("ERROR! File Not Found!");
                reviewFile = null;
                Console.WriteLine("Exiting...");
                Environment.Exit(-1);
            }

            // Variable for holding all values
            var inputFileLines = new List<string>();
            var currentWord = string.Empty;
            while((currentWord = inputFile.ReadLine()) != null) {
                inputFileLines.Add(currentWord);
            }

            // Variables for review file
            string reviewLine;
            var currentSentiment = 0;
            var currentReview = string.Empty;


            // Variables for computing average score
            int totalScore = 0, numFound = 0;

            // Loop over contents of review file
            while ((reviewLine = reviewFile.ReadLine()) != null) {
                currentSentiment = Int32.Parse(reviewLine.Substring(0, reviewLine.IndexOf(' ')));
                currentReview = reviewLine.Substring(reviewLine.IndexOf(' ') + 1);
                // Loop over words in input file
                foreach(var line in inputFileLines) {
                    if (Regex.IsMatch(currentReview, string.Format(@"\b{0}\b", Regex.Escape(line)))) {
                        totalScore += currentSentiment;
                        numFound += 1;
                    }
                }
            }
            // Compute the average score
            double avgScore;
            try {
                avgScore = (double)totalScore / (double)numFound;
            } catch(DivideByZeroException) {
                avgScore = 0;
            }

            Console.WriteLine($"The average score of words in {fileName} is {avgScore:.#####}.");

            // Check overall sentiment of file
            if (avgScore < 1.99) {
                Console.WriteLine($"The overall sentiment of {fileName} is negative.");
            } else {
                Console.WriteLine($"The overall sentiment of {fileName} is positive.");
            }
        }
    }
}
