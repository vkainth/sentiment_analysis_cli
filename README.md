# Sentiment Analysis
This project is a [Nifty Assignment](http://nifty.stanford.edu/) to practice C# programming. It can be used as a general introduction to the language with variables, list operations, foreach loops and other looping mechanisms, exception handling, custom classes, and methods.

## Getting Started
Simply download the project into your system, boot up visual studio, and run the application in visual studio.

## Running the Tests
**Forthcoming**

## Built With
* [Google](http://google.com/)
* [Microsoft C# Documentation](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/index)

## Contributing
Please read [Contributing.md]() for details. **Forthcoming**

## Authors
* [Vibhor Kainth](https://github.com/vibhor-kainth)

## License
This project is licensed under the MIT license - see [LICENSE.md]() for details