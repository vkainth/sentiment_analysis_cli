﻿using System;
using System.Collections.Generic;

namespace sentiment_analysis_cli
{
    public class FileAnalysisWithOutput
    {
        public void calculateSentiment() {
            // Ask user for input file
            Console.Write("Enter the name of a file: ");
            var fileName = Console.ReadLine();
            // Try to open the files
            System.IO.StreamReader userFile, reviewFile;
            try {
                userFile = new System.IO.StreamReader($"../../{fileName}");
                reviewFile = new System.IO.StreamReader(@"../../movieReviews.txt");
            } catch (System.IO.FileNotFoundException) {
                userFile = null;
                reviewFile = null;
                Console.WriteLine("ERROR! Either of the two files not found");
                Console.WriteLine("Exiting...");
                Environment.Exit(-1);
            } catch (ArgumentException) {
                userFile = null;
                reviewFile = null;
                Console.WriteLine("ERROR! Wrong argument supplied!");
                Console.WriteLine($"Name of file {fileName} is problematic.");
                Console.WriteLine("Exiting...");
                Environment.Exit(-2);
            }

            // Move each word from user file into a list
            var userFileList = new List<string>();
            var aWord = string.Empty;
            while ((aWord = userFile.ReadLine()) != null) {
                userFileList.Add(aWord);
            }

            // Move each review, with sentiment, to a list
            var reviewFileList = new List<ReviewDataStruct>();
            var aSentiment = -1;
            var aReview = string.Empty;
            var aLine = string.Empty;
            // Read each line and put into list
            while ((aLine = reviewFile.ReadLine()) != null) {
                if (Int32.TryParse(aLine.Substring(0, aLine.IndexOf(' ')), out aSentiment)) {
                    aReview = aLine.Substring(aLine.IndexOf(' ') + 1);
                    reviewFileList.Add(new ReviewDataStruct(aSentiment, aReview));
                }
            }

            // Calculate sentiment score and number of times word was found
            var score = 0;
            var numFound = 0;
            var avg = 0.0;

            // File streams for writing output
            System.IO.StreamWriter positive = new System.IO.StreamWriter(@"../../positive.txt");
            System.IO.StreamWriter negative = new System.IO.StreamWriter(@"../../negative.txt");

            // Iterate over lists
            foreach(var word in userFileList) {
                score = 0;
                numFound = 0;
                foreach(var reviewObj in reviewFileList) {
                    if (reviewObj.review.Contains(word)) {
                        score += reviewObj.sentiment;
                        numFound += 1;
                    }
                }
                // Try to calculate average
                try {
                    avg = (double)score / (double)numFound;
                } catch(DivideByZeroException) {
                    avg = -1;
                }
                // Check if average is a number
                if (double.IsNaN(avg)) {
                    avg = -1;
                }
                // Filter positive and negative sentiment
                if (avg < 1.9) {
                    Console.WriteLine($"Writing {word} to negative.txt");
                    negative.WriteLine(word);
                } else if (avg > 2.1) {
                    Console.WriteLine($"Writing {word} to positive.txt");
                    positive.WriteLine(word);
                } else {
                    Console.WriteLine($"Found {word} which is neither positive nor negative");
                }
            }
            Console.WriteLine("Storing data in sentiment_analysis_cli/sentiment_analysis_cli/positive.txt and negative,txt");
            negative.Close();
            positive.Close();
        }
    }
}
