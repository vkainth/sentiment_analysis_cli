﻿using System;

namespace sentiment_analysis_cli
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // Declare class variables
            FileAnalysis fileAnalysis = new FileAnalysis();
            WordAnalysis wordAnalysis = new WordAnalysis();
            MostFrequentFileAnalysis mfFileAnalysis = new MostFrequentFileAnalysis();
            FileAnalysisWithOutput fileAnalysisWithOutput = new FileAnalysisWithOutput();

            // Ask user for input and continue until user exits
            var userInput = 0;
            while (userInput != 5) {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1. Get the score of a word");
                Console.WriteLine("2. Get the average score of words in a file");
                Console.WriteLine("3. Find highest/lowest scoring words in a file");
                Console.WriteLine("4. Sort words from file into positive.txt and negative.txt");
                Console.WriteLine("5. Exit the program");
                Console.Write("Enter a number 1-5: ");
                if (Int32.TryParse(Console.ReadLine(), out userInput)) {
                    if (userInput > 5 || userInput < 1)
                    {
                        Console.WriteLine("Wrong input. \nExiting...");
                        Environment.Exit(-3);
                    }
                    switch (userInput)
                    {
                        case 1:
                            wordAnalysis.calculateSentiment();
                            break;
                        case 2:
                            fileAnalysis.calculateFileSentiment();
                            break;
                        case 3:
                            mfFileAnalysis.calculateSentiment();
                            break;
                        case 4:
                            fileAnalysisWithOutput.calculateSentiment();
                            break;
                        case 5:
                            Console.WriteLine("Goodbye.");
                            Environment.Exit(0);
                            break;
                    }
                }
                Console.WriteLine("\n");
            }

        }
    }
}
