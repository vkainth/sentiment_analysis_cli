﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace sentiment_analysis_cli
{
    public class MostFrequentFileAnalysis
    {
        public void calculateSentiment() {
            // Ask user for file
            Console.WriteLine("Enter name of file you want for which you want the average score");
            string fileName = Console.ReadLine();
            // Try to read the file
            System.IO.StreamReader inputFile;
            try
            {
                inputFile = new System.IO.StreamReader($"../../{fileName}");
            }
            catch (System.IO.FileNotFoundException)
            {
                inputFile = null;
                Console.WriteLine("ERROR! File Not Found!");
                Console.WriteLine("Exiting...");
                Environment.Exit(-1);
            }
            // Read file contents
            System.IO.StreamReader reviewFile;
            try
            {
                reviewFile = new System.IO.StreamReader(@"../../movieReviews.txt");
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("ERROR! File Not Found!");
                reviewFile = null;
                Console.WriteLine("Exiting...");
                Environment.Exit(-1);
            }

            // Variable for holding all values in user file
            var inputFileLines = new List<string>();
            var currentWord = string.Empty;
            while ((currentWord = inputFile.ReadLine()) != null)
            {
                inputFileLines.Add(currentWord);
            }


            // Variables for reading review file
            var currentSentiment = 0;
            var currentReview = string.Empty;
            var reviewLine = string.Empty;

            // Variable for holding all values in review file
            var reviewFileLines = new List<ReviewDataStruct>();
            while ((reviewLine = reviewFile.ReadLine()) != null) {
                if (Int32.TryParse(reviewLine.Substring(0, reviewLine.IndexOf(' ')), out currentSentiment)) {
                    currentReview = reviewLine.Substring(reviewLine.IndexOf(' ') + 1);
                    reviewFileLines.Add(new ReviewDataStruct(currentSentiment, currentReview));
                }
            }

            // Variable to store maximum sentiment and the corresponding word
            var maxPosWord = string.Empty;
            var maxNegWord = string.Empty;
            var totalScore = 0;
            var numFound = 0;

            // Variable to find average
            var posAvg = 0.0;
            var negAvg = 2.0;
            var avg = 0.0;

            // Loop over each word in user file
            foreach(var word in inputFileLines) {
                // Reset total score and number found
                totalScore = 0;
                numFound = 0;
                reviewFile.DiscardBufferedData();
                // Loop over each line in review file
                foreach(var reviewObj in reviewFileLines)
                {
                    if (reviewObj.review.Contains(word))
                    {
                        totalScore += reviewObj.sentiment;
                        numFound += 1;
                    }
                }
                // Calculate average
                try {
                    avg = (double)totalScore / (double)numFound;
                } catch(DivideByZeroException) {
                    avg = 0.0;
                }
                if (double.IsNaN(avg)) {
                    avg = -1.0;
                }
                // Check if total score is greater than threshold
                if (avg >= 1.99 && avg > posAvg) {
                    maxPosWord = word;
                    posAvg = avg;
                } else if (avg < 1.99 && avg < negAvg && avg > -1.0) {
                    maxNegWord = word;
                    negAvg = avg;
                }
            }
            // Display Results
            Console.WriteLine($"The most positive word, with a score of {posAvg} is {maxPosWord}");
            Console.WriteLine($"The most negative word, with a score of {negAvg} is {maxNegWord}");
        }
    }
}
